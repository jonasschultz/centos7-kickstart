## Kickstart file for home lab. 

I actually use this together with a different project, but this one is public so I can manage without needing 
to have any sensitive information in public.

(This one is based off [Archoniks Centos 7 Kickstart Example](https://github.com/archonik/centos-7-kickstart-example)

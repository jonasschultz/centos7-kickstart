yum -y install yum-utils wget unzip nano mod_ssl python-certbot-apache mariadb-server
rpm -Uvh http://rpms.remirepo.net/enterprise/remi-release-7.rpm
yum-config-manager --enable --remi-php71
yum -y install php php-mysqlnd php-pecl-zip php-xml php-mbstring php-gd php-mcrypt php-pear php-pspell php-pdo php-xml php-intl php-zip php-zlib
sed -i "s/post_max_size = 8M/post_max_size = 256M/" /etc/php.ini
sed -i "s/upload_max_filesize = 2M/upload_max_filesize = 256M/" /etc/php.ini
systemctl restart httpd.service
wget https://download.owncloud.org/community/owncloud-10.0.9.zip
unzip -d /var/www/html/ owncloud-10.0.9.zip

mysql -u root <<-EOF
UPDATE mysql.user SET Password=PASSWORD('$rootpass') WHERE User='root';
DELETE FROM mysql.user WHERE User='root' AND Host NOT IN ('localhost', '127.0.0.1', '::1');
DELETE FROM mysql.user WHERE User='';
DELETE FROM mysql.db WHERE Db='test' OR Db='test_%';
CREATE DATABASE ownclouddb;
GRANT ALL PRIVILEGES ON ownclouddb.* TO 'ownclouduser'@'localhost' IDENTIFIED BY 'Pajjas07' WITH GRANT OPTION;
FLUSH PRIVILEGES;
EOF

cat <<EOCFG >> /etc/httpd/conf.d/owncloud.conf
<VirtualHost 172.18.74.108:80>
ServerAdmin webmaster@yourdomain.com
DocumentRoot "/var/www/html/owncloud/"
ServerName owncloud.local

ErrorLog "/var/log/httpd/owncloud.local-error_log"
CustomLog "/var/log/httpd/owncloud.local-access_log" combined

<Directory "/var/www/html/owncloud/">
DirectoryIndex index.html index.php
Options FollowSymLinks
AllowOverride All
Require all granted
</Directory>
</VirtualHost>
EOCFG

mkdir -p /var/www/html/owncloud/data
chown -R /var/www/html/owncloud/

systemctl enable httpd
systemctl enable mariadb

certbot --apache -d owncloud.local --register-unsafely-without-email

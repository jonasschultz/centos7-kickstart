#version=RHEL7
install
text
lang en_US.UTF-8
keyboard se
timezone Europe/Stockholm
auth --useshadow --passalgo=sha256
firewall --disabled
selinux --disabled
services --enabled=sshd
eula --agreed

%pre
#!/bin/bash
DISK=$(lsblk --output NAME,TYPE | grep disk | head -n1 | cut -d" " -f1)
cat > /tmp/setup << EOF
ignoredisk --only-use=$DISK
clearpart --drives=$DISK --all --initlabel
bootloader --location=mbr --boot-drive=$DISK
zerombr
part swap --asprimary --fstype="swap" --ondisk=$DISK --recommended
part /boot --fstype ext4 --ondisk=$DISK --recommended
part / --fstype ext4 --ondisk=$DISK --size 8192 --grow
EOF

NET_CFG=$(grep -oE 'net_cfg=#.*#' < /proc/cmdline)
HOST_NAME=${HOST_NAME##*=}
if [ ! -z "$NET_CFG" ]; then
  NET_CFG=${NET_CFG#*=}
  echo "${NET_CFG//#/}" >> /tmp/setup
fi

ACTION=$(grep -oE 'action=[a-z]+' < /proc/cmdline)
if [ -z "$ACTION" ]; then
  echo "poweroff" >> /tmp/setup
else
  case "${ACTION##*=}" in
    "reboot")
      echo "${ACTION##*=}" >> /tmp/setup
    ;;
    *)
      echo "poweroff" >> /tmp/setup
    ;;
  esac
fi
%end

%include /tmp/setup

rootpw --plaintext centos

## repos
repo --name=updates --baseurl=http://ftp.hosteurope.de/mirror/centos.org/7/updates/x86_64/
repo --name=extras --baseurl=http://ftp.hosteurope.de/mirror/centos.org/7/extras/x86_64/
repo --name=epel --baseurl=https://dl.fedoraproject.org/pub/epel/7/x86_64/

## network install mirror
url --url="http://ftp.hosteurope.de/mirror/centos.org/7/os/x86_64/"

%packages --ignoremissing --excludedocs
@core --nodefaults
bash-completion
epel-release
deltarpm
wget
nano
unzip
-NetworkManager*
-aic94xx-firmware*
-alsa-*
-iwl*firmware
-ql*firmware
-ivtv*
-plymouth*
-kexec-tools
-dracut-network
-btrfs-progs*
-postfix
%end

%post
#!/bin/bash

##
## Ansible: Add SSH Key (Optional)
##

mkdir -m 700 -p /root/.ssh
install -b -m 600 /dev/null /root/.ssh/authorized_keys
cat > /root/.ssh/authorized_keys << EOF
ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQCsbTSk6VreMtVBciy7fOBZkI1YcUkgVXzbw2CRBVNJVqpCHqkv0uouivjm07xJDmq9ngozrHglLzMkC9f4K5YQcbye4+DjQrMQgj7fnlI1meMV42yNmSpl/w5qEl84yXRfoP1UUqmZnm1HICAfZZlRxB6V4n3qgk0pL3WdAXrFUvHddDaSdOo/u4L0YKKCTV0l0H9Y2cdDRIsjnkx63886pY3xPKDt6Asltve+gHsBm4e1hdz39H3ZT25xl5pTs6Xc1uc4FyHS5+zmcefimiGNWc5PymCa3dTFQGNf+mymRN0O2Rvo8rYlu8ZUhAt8LEgzBmYQbUCFl9drp1z7YYRt
EOF
yum -y install python

##
## END: Ansible
##

##
## Cleanup
##

yum clean all
fstrim /

##
## END: Cleanup
##

%end